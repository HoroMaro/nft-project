jQuery(document).ready(function ($){
    let toggler = $('header .open'),
        content = $('header nav');

    toggler.on('click', function (e){
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            content.addClass('active').fadeIn();
        } else {
            content.removeClass('active').fadeOut();
        }
    })
})