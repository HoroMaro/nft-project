<?php
/*
 * Template Name: Example
 */
?>

<?php get_header(); ?>

<?php
$cardBlock = get_field('cards');
?>

<section class="cards">
    <?php foreach ($cardBlock as $item): ?>
        <div class="card">
            <a href="<?= $item['card']['link']['url'] ?>" target="<?= $item['card']['link']['target'] ?>"><?= $item['card']['link']['title'] ?></a>
            <div>
                <img src="<?= $item['card']['image'] ?>" alt="">
            </div>
                <?php if (!empty($item['card']['title'])): ?>
                    <h2>Name: <?= $item['card']['title'] ?></h2>
                <?php endif; ?>
            <h3>QTY: <?= $item['card']['qty'] ?></h3>
            <h2>Price: <?= $item['card']['price'] ?></h2>
        </div>
    <?php endforeach; ?>
</section>

<?php get_footer(); ?>
