<?php get_template_part('inc/frontend/headers/global-header', '', (!empty($args)) ? $args : []); ?>

<?php
$headerBlock = get_field('header', 'option');
?>

<header>
    <div class="header">
        <div class="wrapper">
            <div>
                <img src="<?= $headerBlock['logo'] ?>" alt="">
            </div>
            <ul>
                <?php foreach ($headerBlock['navigation_items'] as $nav): ?>
                    <li><a href="<?= $nav['element']['url'] ?>"
                           target="<?= $nav['element']['target'] ?>"><?= $nav['element']['title'] ?></a></li>
                <?php endforeach; ?>
            </ul>
            <?php if ($headerBlock['display_loginsign-up'] === true): ?>
                <span>
                    <a href="<?= $headerBlock['login_button']['url'] ?>"
                       target="<?= $headerBlock['login_button']['target'] ?>"><?= $headerBlock['login_button']['title'] ?></a>
                    /
                    <a href="<?= $headerBlock['sign-up_button']['url'] ?>"
                       target="<?= $headerBlock['sign-up_button']['target'] ?>"><?= $headerBlock['sign-up_button']['title'] ?></a>
                </span>
            <?php endif; ?>
            <a href="<?= $headerBlock['button']['url'] ?>"
               target="<?= $headerBlock['button']['target'] ?>" class="btn"><?= $headerBlock['button']['title'] ?></a>
        </div>
    </div>

    <div class="header-mobile">
        <div class="main-content">
            <div class="open">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div>
                <img src="<?= $headerBlock['logo'] ?>" alt="">
            </div>
            <a href="<?= $headerBlock['login_button']['url'] ?>"
               target="<?= $headerBlock['login_button']['target'] ?>" class="btn"><?= $headerBlock['login_button']['title'] ?></a>
        </div>
        <nav>
            <ul>
                <?php foreach ($headerBlock['navigation_items'] as $nav): ?>
                    <li><a href="<?= $nav['element']['url'] ?>"
                           target="<?= $nav['element']['target'] ?>"><?= $nav['element']['title'] ?></a></li>
                <?php endforeach; ?>
            </ul>
            <a href="<?= $headerBlock['button']['url'] ?>"
               target="<?= $headerBlock['button']['target'] ?>" class="btn"><?= $headerBlock['button']['title'] ?></a>
        </nav>
    </div>
</header>
